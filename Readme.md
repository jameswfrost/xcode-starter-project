To create a new starter project, run the following from your terminal:

`ruby -e "$(curl -fsSL http://tinyurl.com/moo-xcode-starter-app)"`

The script will be downloaded, ask you a couple of questions, and create and open a new Xcode project.

Current defaults set up in the project include:

* A Podfile, and automatically running `pod install` to generate a workspace. Currently pulls in AFNetworking by default.
* A sensible project structure including MVC class directories
* A readme file
* Directories for Provisioning and Certificates
* A Rakefile (currently empty) for building the project from the commandline or Jenkins
* Agvtool versioning set up
* Localizable.strings file
* Deployment target set to 5.1
