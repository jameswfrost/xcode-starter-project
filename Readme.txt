# StarterApp

_Description: What does this project do and who does it serve?_

## Project Setup

_How do I, as a developer, start working on the project?_ 

## Testing

_How do I run the project's automated tests?_

### Unit Tests

### Integration Tests

## Deploying

### _How to setup the deployment environment_

- _Required environment variables or credentials not included in git._
- _Cocoapods / third party library information._

### _How to deploy_

## Troubleshooting & Useful Tools

_Examples of common tasks_