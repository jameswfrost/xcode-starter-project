//
//  MOOAppDelegate.h
//
//  Copyright (c) 2013 Mubaloo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
