//
//  main.m
//  StarterApp
//
//  Created by James Frost on 16/03/2013.
//  Copyright (c) 2013 Mubaloo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MOOAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MOOAppDelegate class]));
    }
}
