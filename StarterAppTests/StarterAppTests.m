//
//  StarterAppTests.m
//
//  Copyright (c) 2013 Mubaloo. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>


@interface StarterAppTests : SenTestCase
@end


@implementation StarterAppTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in StarterAppTests");
}

@end
