#!/System/Library/Frameworks/Ruby.framework/Versions/1.8/usr/bin/ruby

REPO_LOCATION = "https://jameswfrost@bitbucket.org/jameswfrost/xcode-starter-project.git"
require 'tmpdir'

module Tty extend self
  def blue; bold 34; end
  def white; bold 39; end
  def red; underline 31; end
  def reset; escape 0; end
  def bold n; escape "1;#{n}" end
  def underline n; escape "4;#{n}" end
  def escape n; "\033[#{n}m" if STDOUT.tty? end
end

class Array
  def shell_s
    cp = dup
    first = cp.shift
    cp.map{ |arg| arg.gsub " ", "\\ " }.unshift(first) * " "
  end
end

def print_with_pointer *args
  puts "#{Tty.blue}==>#{Tty.white} #{args.shell_s}#{Tty.reset}"
end

def warn warning
  puts "#{Tty.red}Warning#{Tty.reset}: #{warning.chomp}"
end

def system *args
  abort "Failed during: #{args.shell_s}" unless Kernel.system *args
end

def git
  @git ||= if ENV['GIT'] and File.executable? ENV['GIT']
    ENV['GIT']
  elsif Kernel.system '/usr/bin/which -s git'
    'git'
  else
    s = `xcrun -find git 2>/dev/null`.chomp
    s if $? and not s.empty?
  end
end

def input(prompt)
  puts prompt
  gets.chomp
end

def ask(question)
  answer = input("#{question} [yn]") 
  return (answer == "y" || answer == "Y")
end

def copy_to_destination(source, destination)
  # First copy everything over
  FileUtils.cp_r(File.join(source, "."), destination)

  # Then delete the git directory and any gitkeeps
  FileUtils.remove_dir(File.join(destination, ".git"))
  Dir.glob(File.join(destination, '**/.gitkeep'), File::FNM_DOTMATCH).each { |f| File.delete(f) }

  # Also delete this script and the readme for it
  FileUtils.rm File.join(destination, "go")
  FileUtils.rm File.join(destination, "Readme.md")
end

def rename_app(project_name, prefix)
  # First, just directories
  Dir.glob(File.join(project_name, '**/*StarterApp*/')).each do |f|
    File.rename(f, f.sub(/StarterApp/, project_name))
  end

  # Then files
  Dir.glob(File.join(project_name, '**/*StarterApp*')).each do |f|
    File.rename(f, f.sub(/StarterApp/, project_name))
  end

  Dir.glob(File.join(project_name, '**/MOO*')).each do |f|
    File.rename(f, f.sub(/MOO/, prefix))
  end

  # Then text within files
  Dir.glob(File.join(project_name, '**/*.{m,h,pch,plist,xcworkspacedata,pbxproj}')).each do |f|
    if File.file? f
      contents = File.read(f)
      contents.gsub!(/StarterApp/, project_name)
      contents.gsub!(/MOO/, prefix)
      File.open(f, "w") { |f| f.write contents }
    end
  end
end

def install_cocoapods_if_required
  if ask("Should I run `pod install` for you?") then
    system "pod install"                                                                          
    system "open", "#{PROJECT_NAME}.xcworkspace"
  else
    system "open", "#{PROJECT_NAME}.xcodeproj"
  end
end

print_with_pointer "Xcode Starter Project"
PROJECT_NAME = input "What's the name of the new project? "
CLASS_PREFIX = input "What should I set the class prefix to? (e.g. MOO) "

print_with_pointer "Downloading latest version of the template project..."

Dir.mktmpdir('xcode-starter') do |dir|
  if git 
    system git, "clone", REPO_LOCATION, dir
    begin
      Dir.mkdir(PROJECT_NAME)
    rescue
      continue = ask "#{PROJECT_NAME} directory already exists. Continue?"
      abort unless continue
    end
    print_with_pointer "Copying files to target directory..."
    copy_to_destination dir, PROJECT_NAME
    print_with_pointer "Renaming files..."
    rename_app PROJECT_NAME, CLASS_PREFIX
    print_with_pointer "Updating class prefix..."
    Dir.chdir PROJECT_NAME
    install_cocoapods_if_required
  end
end

print_with_pointer "All done!"
